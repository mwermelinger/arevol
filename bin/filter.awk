/^COMPONENT/ {
  print $1, $2
}
/DEPENDS/ {
  print $1, $2, $3
}
/PROVIDES/ {
  print $1, $2, $3
}
/REQUIRES/ {
  print $1, $2, $3
}
/^FEATURE/ {
  print $1, $2
}
/CONTAINS_FEATURE/ {
  print $1, $2, $3
}
/CONTAINS_COMPONENT/ {
  print $1, $2, $3
}

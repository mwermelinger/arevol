BEGIN { l = 0;}
{
   tags[l] = $2;
   l++;
}
END {
   for (i = 0; i< l-1; i++ ) {
     print "NEXT " tags[i] " " tags[i+1];
   }
}

# ArEvol

This repository contains data and scripts for empirical research on the
architectural evolution of two projects, Eclipse and NetBeans, 
that have adopted and adapted the OSGi component model.

We provide primary and secondary data. 
The primary data consists of the `manifest.mf` and XML configuration files 
taken from various builds (mostly major and minor releases) of the two projects, 
spanning more than 10 years for each one. 
The secondary data consists of text files representing in a relational way 
the architectural components and dependencies in each build of each project.

The scripts, which require Linux and [Crocopat](http://www.sosy-lab.org/~dbeyer/CrocoPat/), 
extract from the secondary data an architectural evolution model for a given sequence of builds. 
For non-Linux users
we recommend to first install [docker](https://docs.docker.com/installation/), then run the `bin/d` 
script, to make sure the precompiled `crocopat` command can be run.

Among other things, the extracted architectural evolution model can be used for visualisation, and for 
computing trends over time of cohesion, coupling and other metrics; 
see [*Assessing architectural evolution: a case study*](http://oro.open.ac.uk/28753/).

ArEvol is further explained in the paper 
[*An Architectural Evolution Dataset*](http://oro.open.ac.uk/42318). 

If you use ArEvol in your own research, please cite that paper. 
We'll be happy to add your own research based on ArEvol to this page: 
please submit a request via the issue tracker.

We welcome further datasets (not necessarily based on OSGi) and analysis scripts. 
Please create one folder per project for the data, 
put your scripts in the `contribs` folder and submit a pull request. 

Michel Wermelinger & Yijun Yu


DEPENDS org.eclipse.ant.core org.apache.xerces
DEPENDS org.eclipse.ant.core org.eclipse.core.resources
DEPENDS org.eclipse.ant.ui org.eclipse.ant.core
DEPENDS org.eclipse.ant.ui org.eclipse.core.resources
DEPENDS org.eclipse.ant.ui org.eclipse.ui
DEPENDS org.eclipse.compare org.eclipse.core.resources
DEPENDS org.eclipse.compare org.eclipse.ui
DEPENDS org.eclipse.core.resources org.apache.xerces
DEPENDS org.eclipse.core.runtime org.apache.xerces
DEPENDS org.eclipse.core.target org.apache.xerces
DEPENDS org.eclipse.core.target org.eclipse.core.resources
DEPENDS org.eclipse.core.target org.eclipse.webdav
DEPENDS org.eclipse.debug.core org.apache.xerces
DEPENDS org.eclipse.debug.core org.eclipse.core.resources
DEPENDS org.eclipse.debug.ui org.apache.xerces
DEPENDS org.eclipse.debug.ui org.eclipse.core.resources
DEPENDS org.eclipse.debug.ui org.eclipse.debug.core
DEPENDS org.eclipse.debug.ui org.eclipse.ui
DEPENDS org.eclipse.help org.apache.xerces
DEPENDS org.eclipse.help org.eclipse.core.runtime
DEPENDS org.eclipse.help.ui org.apache.xerces
DEPENDS org.eclipse.help.ui org.eclipse.core.resources
DEPENDS org.eclipse.help.ui org.eclipse.core.runtime
DEPENDS org.eclipse.help.ui org.eclipse.help
DEPENDS org.eclipse.help.ui org.eclipse.ui
DEPENDS org.eclipse.jdt.core org.apache.xerces
DEPENDS org.eclipse.jdt.core org.eclipse.core.resources
DEPENDS org.eclipse.jdt.debug org.apache.xerces
DEPENDS org.eclipse.jdt.debug org.eclipse.core.resources
DEPENDS org.eclipse.jdt.debug org.eclipse.debug.core
DEPENDS org.eclipse.jdt.debug org.eclipse.jdt.core
DEPENDS org.eclipse.jdt.launching org.apache.xerces
DEPENDS org.eclipse.jdt.launching org.eclipse.core.resources
DEPENDS org.eclipse.jdt.launching org.eclipse.debug.core
DEPENDS org.eclipse.jdt.launching org.eclipse.jdt.core
DEPENDS org.eclipse.jdt.launching org.eclipse.jdt.debug
DEPENDS org.eclipse.jdt.ui org.apache.xerces
DEPENDS org.eclipse.jdt.ui org.eclipse.compare
DEPENDS org.eclipse.jdt.ui org.eclipse.core.resources
DEPENDS org.eclipse.jdt.ui org.eclipse.debug.core
DEPENDS org.eclipse.jdt.ui org.eclipse.debug.ui
DEPENDS org.eclipse.jdt.ui org.eclipse.jdt.core
DEPENDS org.eclipse.jdt.ui org.eclipse.jdt.debug
DEPENDS org.eclipse.jdt.ui org.eclipse.jdt.launching
DEPENDS org.eclipse.jdt.ui org.eclipse.search
DEPENDS org.eclipse.jdt.ui org.eclipse.ui
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.core.resources
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.jdt.core
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.jdt.ui
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.ui
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.vcm.core
DEPENDS org.eclipse.jdt.ui.vcm org.eclipse.vcm.ui
DEPENDS org.eclipse.pde org.apache.xerces
DEPENDS org.eclipse.pde org.eclipse.ant.core
DEPENDS org.eclipse.pde org.eclipse.ant.ui
DEPENDS org.eclipse.pde org.eclipse.core.resources
DEPENDS org.eclipse.pde org.eclipse.core.runtime
DEPENDS org.eclipse.pde org.eclipse.debug.core
DEPENDS org.eclipse.pde org.eclipse.debug.ui
DEPENDS org.eclipse.pde org.eclipse.jdt.core
DEPENDS org.eclipse.pde org.eclipse.jdt.launching
DEPENDS org.eclipse.pde org.eclipse.jdt.ui
DEPENDS org.eclipse.pde org.eclipse.pde.core
DEPENDS org.eclipse.pde org.eclipse.ui
DEPENDS org.eclipse.pde.core org.apache.xerces
DEPENDS org.eclipse.pde.core org.eclipse.ant.core
DEPENDS org.eclipse.pde.core org.eclipse.jdt.core
DEPENDS org.eclipse.pde.runtime org.apache.xerces
DEPENDS org.eclipse.pde.runtime org.eclipse.core.resources
DEPENDS org.eclipse.pde.runtime org.eclipse.core.runtime
DEPENDS org.eclipse.pde.runtime org.eclipse.ui
DEPENDS org.eclipse.scripting org.apache.xerces
DEPENDS org.eclipse.scripting org.eclipse.core.resources
DEPENDS org.eclipse.scripting org.eclipse.ui
DEPENDS org.eclipse.scripting.adapters.javascript org.eclipse.scripting
DEPENDS org.eclipse.search org.apache.xerces
DEPENDS org.eclipse.search org.eclipse.core.resources
DEPENDS org.eclipse.search org.eclipse.ui
DEPENDS org.eclipse.ui org.apache.xerces
DEPENDS org.eclipse.ui org.eclipse.core.resources
DEPENDS org.eclipse.ui org.eclipse.help
DEPENDS org.eclipse.ui org.eclipse.swt
DEPENDS org.eclipse.update org.eclipse.core.runtime
DEPENDS org.eclipse.update org.eclipse.ui
DEPENDS org.eclipse.update org.eclipse.webdav
DEPENDS org.eclipse.vcm.core org.apache.xerces
DEPENDS org.eclipse.vcm.core org.eclipse.core.resources
DEPENDS org.eclipse.vcm.core.cvs org.eclipse.core.resources
DEPENDS org.eclipse.vcm.core.cvs org.eclipse.vcm.core
DEPENDS org.eclipse.vcm.core.cvs.ssh org.eclipse.core.resources
DEPENDS org.eclipse.vcm.core.cvs.ssh org.eclipse.vcm.core
DEPENDS org.eclipse.vcm.core.cvs.ssh org.eclipse.vcm.core.cvs
DEPENDS org.eclipse.vcm.ui org.eclipse.compare
DEPENDS org.eclipse.vcm.ui org.eclipse.core.resources
DEPENDS org.eclipse.vcm.ui org.eclipse.ui
DEPENDS org.eclipse.vcm.ui org.eclipse.vcm.core
DEPENDS org.eclipse.vcm.ui.cvs org.eclipse.core.resources
DEPENDS org.eclipse.vcm.ui.cvs org.eclipse.ui
DEPENDS org.eclipse.vcm.ui.cvs org.eclipse.vcm.core
DEPENDS org.eclipse.vcm.ui.cvs org.eclipse.vcm.core.cvs
DEPENDS org.eclipse.vcm.ui.cvs org.eclipse.vcm.ui
DEPENDS org.eclipse.webdav org.apache.xerces
COMPONENT org.apache.xerces
COMPONENT org.eclipse.ant.core
COMPONENT org.eclipse.ant.ui
COMPONENT org.eclipse.compare
COMPONENT org.eclipse.core.boot
COMPONENT org.eclipse.core.resources
COMPONENT org.eclipse.core.runtime
COMPONENT org.eclipse.core.target
COMPONENT org.eclipse.debug.core
COMPONENT org.eclipse.debug.ui
COMPONENT org.eclipse.help
COMPONENT org.eclipse.help.ui
COMPONENT org.eclipse.jdt.core
COMPONENT org.eclipse.jdt.debug
COMPONENT org.eclipse.jdt.doc.isv
COMPONENT org.eclipse.jdt.doc.user
COMPONENT org.eclipse.jdt.launching
COMPONENT org.eclipse.jdt.ui
COMPONENT org.eclipse.jdt.ui.vcm
COMPONENT org.eclipse.pde
COMPONENT org.eclipse.pde.core
COMPONENT org.eclipse.pde.doc.user
COMPONENT org.eclipse.pde.runtime
COMPONENT org.eclipse.platform.doc.isv
COMPONENT org.eclipse.platform.doc.user
COMPONENT org.eclipse.scripting
COMPONENT org.eclipse.scripting.adapters.javascript
COMPONENT org.eclipse.sdk
COMPONENT org.eclipse.search
COMPONENT org.eclipse.swt
COMPONENT org.eclipse.ui
COMPONENT org.eclipse.update
COMPONENT org.eclipse.vcm.core
COMPONENT org.eclipse.vcm.core.cvs
COMPONENT org.eclipse.vcm.core.cvs.ssh
COMPONENT org.eclipse.vcm.ui
COMPONENT org.eclipse.vcm.ui.cvs
COMPONENT org.eclipse.webdav
PROVIDES org.eclipse.ant.core org.eclipse.ant.core.antObjects
PROVIDES org.eclipse.ant.core org.eclipse.ant.core.antTasks
PROVIDES org.eclipse.ant.core org.eclipse.ant.core.antTypes
PROVIDES org.eclipse.compare org.eclipse.compare.contentMergeViewers
PROVIDES org.eclipse.compare org.eclipse.compare.contentViewers
PROVIDES org.eclipse.compare org.eclipse.compare.structureCreators
PROVIDES org.eclipse.compare org.eclipse.compare.structureMergeViewers
PROVIDES org.eclipse.core.resources org.eclipse.core.resources.builders
PROVIDES org.eclipse.core.resources org.eclipse.core.resources.internalFileValidator
PROVIDES org.eclipse.core.resources org.eclipse.core.resources.markers
PROVIDES org.eclipse.core.resources org.eclipse.core.resources.natures
PROVIDES org.eclipse.core.runtime org.eclipse.core.runtime.applications
PROVIDES org.eclipse.core.runtime org.eclipse.core.runtime.urlHandlers
PROVIDES org.eclipse.core.target org.eclipse.core.target.targets
PROVIDES org.eclipse.debug.core org.eclipse.debug.core.launchers
PROVIDES org.eclipse.debug.ui org.eclipse.debug.ui.debugModelPresentations
PROVIDES org.eclipse.help org.eclipse.help.contexts
PROVIDES org.eclipse.help org.eclipse.help.contributions
PROVIDES org.eclipse.help org.eclipse.help.searchEngine
PROVIDES org.eclipse.help org.eclipse.help.support
PROVIDES org.eclipse.jdt.launching org.eclipse.jdt.launching.vmInstallTypes
PROVIDES org.eclipse.jdt.ui org.eclipse.jdt.ui.javaElementFilters
PROVIDES org.eclipse.pde org.eclipse.pde.newExtension
PROVIDES org.eclipse.pde org.eclipse.pde.projectGenerators
PROVIDES org.eclipse.pde org.eclipse.pde.schemaMap
PROVIDES org.eclipse.search org.eclipse.search.searchPages
PROVIDES org.eclipse.search org.eclipse.search.searchResultSorters
PROVIDES org.eclipse.ui org.eclipse.ui.actionSets
PROVIDES org.eclipse.ui org.eclipse.ui.documentProviders
PROVIDES org.eclipse.ui org.eclipse.ui.dropActions
PROVIDES org.eclipse.ui org.eclipse.ui.editorActions
PROVIDES org.eclipse.ui org.eclipse.ui.editors
PROVIDES org.eclipse.ui org.eclipse.ui.elementFactories
PROVIDES org.eclipse.ui org.eclipse.ui.exportWizards
PROVIDES org.eclipse.ui org.eclipse.ui.importWizards
PROVIDES org.eclipse.ui org.eclipse.ui.markerImageProviders
PROVIDES org.eclipse.ui org.eclipse.ui.markerUpdaters
PROVIDES org.eclipse.ui org.eclipse.ui.newWizards
PROVIDES org.eclipse.ui org.eclipse.ui.perspectiveExtensions
PROVIDES org.eclipse.ui org.eclipse.ui.perspectives
PROVIDES org.eclipse.ui org.eclipse.ui.popupMenus
PROVIDES org.eclipse.ui org.eclipse.ui.preferencePages
PROVIDES org.eclipse.ui org.eclipse.ui.projectNatureImages
PROVIDES org.eclipse.ui org.eclipse.ui.propertyPages
PROVIDES org.eclipse.ui org.eclipse.ui.resourceFilters
PROVIDES org.eclipse.ui org.eclipse.ui.viewActions
PROVIDES org.eclipse.ui org.eclipse.ui.views
PROVIDES org.eclipse.vcm.core org.eclipse.vcm.core.adapters
PROVIDES org.eclipse.vcm.core org.eclipse.vcm.core.ignore
PROVIDES org.eclipse.vcm.core.cvs org.eclipse.vcm.core.cvs.authenticator
PROVIDES org.eclipse.vcm.core.cvs org.eclipse.vcm.core.cvs.connectionmethods
PROVIDES org.eclipse.vcm.ui org.eclipse.vcm.ui.adapters
REQUIRES org.eclipse.ant.core org.eclipse.ant.core.antTasks
REQUIRES org.eclipse.ant.core org.eclipse.ant.core.antTypes
REQUIRES org.eclipse.ant.core org.eclipse.core.runtime.applications
REQUIRES org.eclipse.ant.ui org.eclipse.ui.popupMenus
REQUIRES org.eclipse.ant.ui org.eclipse.ui.views
REQUIRES org.eclipse.compare org.eclipse.compare.contentMergeViewers
REQUIRES org.eclipse.compare org.eclipse.compare.contentViewers
REQUIRES org.eclipse.compare org.eclipse.compare.structureCreators
REQUIRES org.eclipse.compare org.eclipse.ui.editors
REQUIRES org.eclipse.compare org.eclipse.ui.popupMenus
REQUIRES org.eclipse.compare org.eclipse.ui.preferencePages
REQUIRES org.eclipse.core.resources org.eclipse.core.resources.markers
REQUIRES org.eclipse.core.target org.eclipse.core.target.targets
REQUIRES org.eclipse.debug.core org.eclipse.core.resources.markers
REQUIRES org.eclipse.debug.ui org.eclipse.core.resources.markers
REQUIRES org.eclipse.debug.ui org.eclipse.ui.actionSets
REQUIRES org.eclipse.debug.ui org.eclipse.ui.perspectives
REQUIRES org.eclipse.debug.ui org.eclipse.ui.preferencePages
REQUIRES org.eclipse.debug.ui org.eclipse.ui.propertyPages
REQUIRES org.eclipse.debug.ui org.eclipse.ui.views
REQUIRES org.eclipse.help org.eclipse.core.runtime.applications
REQUIRES org.eclipse.help.ui org.eclipse.core.runtime.applications
REQUIRES org.eclipse.help.ui org.eclipse.help.contexts
REQUIRES org.eclipse.help.ui org.eclipse.help.support
REQUIRES org.eclipse.help.ui org.eclipse.ui.actionSets
REQUIRES org.eclipse.help.ui org.eclipse.ui.perspectives
REQUIRES org.eclipse.help.ui org.eclipse.ui.preferencePages
REQUIRES org.eclipse.help.ui org.eclipse.ui.views
REQUIRES org.eclipse.jdt.core org.eclipse.core.resources.builders
REQUIRES org.eclipse.jdt.core org.eclipse.core.resources.markers
REQUIRES org.eclipse.jdt.core org.eclipse.core.resources.natures
REQUIRES org.eclipse.jdt.debug org.eclipse.core.resources.markers
REQUIRES org.eclipse.jdt.doc.isv org.eclipse.help.contributions
REQUIRES org.eclipse.jdt.doc.user org.eclipse.help.contexts
REQUIRES org.eclipse.jdt.doc.user org.eclipse.help.contributions
REQUIRES org.eclipse.jdt.ui org.eclipse.compare.contentMergeViewers
REQUIRES org.eclipse.jdt.ui org.eclipse.compare.contentViewers
REQUIRES org.eclipse.jdt.ui org.eclipse.compare.structureCreators
REQUIRES org.eclipse.jdt.ui org.eclipse.core.resources.markers
REQUIRES org.eclipse.jdt.ui org.eclipse.debug.core.launchers
REQUIRES org.eclipse.jdt.ui org.eclipse.debug.ui.debugModelPresentations
REQUIRES org.eclipse.jdt.ui org.eclipse.jdt.launching.vmInstallTypes
REQUIRES org.eclipse.jdt.ui org.eclipse.jdt.ui.javaElementFilters
REQUIRES org.eclipse.jdt.ui org.eclipse.search.searchPages
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.actionSets
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.editors
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.elementFactories
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.exportWizards
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.markerUpdaters
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.newWizards
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.perspectiveExtensions
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.perspectives
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.popupMenus
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.preferencePages
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.projectNatureImages
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.propertyPages
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.resourceFilters
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.viewActions
REQUIRES org.eclipse.jdt.ui org.eclipse.ui.views
REQUIRES org.eclipse.jdt.ui.vcm org.eclipse.ui.popupMenus
REQUIRES org.eclipse.jdt.ui.vcm org.eclipse.ui.propertyPages
REQUIRES org.eclipse.jdt.ui.vcm org.eclipse.ui.viewActions
REQUIRES org.eclipse.jdt.ui.vcm org.eclipse.vcm.core.ignore
REQUIRES org.eclipse.pde org.eclipse.core.resources.builders
REQUIRES org.eclipse.pde org.eclipse.core.resources.natures
REQUIRES org.eclipse.pde org.eclipse.debug.core.launchers
REQUIRES org.eclipse.pde org.eclipse.pde.newExtension
REQUIRES org.eclipse.pde org.eclipse.pde.projectGenerators
REQUIRES org.eclipse.pde org.eclipse.pde.schemaMap
REQUIRES org.eclipse.pde org.eclipse.ui.editors
REQUIRES org.eclipse.pde org.eclipse.ui.newWizards
REQUIRES org.eclipse.pde org.eclipse.ui.perspectives
REQUIRES org.eclipse.pde org.eclipse.ui.popupMenus
REQUIRES org.eclipse.pde org.eclipse.ui.preferencePages
REQUIRES org.eclipse.pde.core org.eclipse.ant.core.antObjects
REQUIRES org.eclipse.pde.core org.eclipse.ant.core.antTasks
REQUIRES org.eclipse.pde.doc.user org.eclipse.help.contributions
REQUIRES org.eclipse.pde.runtime org.eclipse.ui.propertyPages
REQUIRES org.eclipse.pde.runtime org.eclipse.ui.views
REQUIRES org.eclipse.platform.doc.isv org.eclipse.help.contributions
REQUIRES org.eclipse.platform.doc.user org.eclipse.help.contexts
REQUIRES org.eclipse.platform.doc.user org.eclipse.help.contributions
REQUIRES org.eclipse.scripting org.eclipse.ui.editors
REQUIRES org.eclipse.scripting org.eclipse.ui.perspectives
REQUIRES org.eclipse.scripting org.eclipse.ui.preferencePages
REQUIRES org.eclipse.scripting org.eclipse.ui.views
REQUIRES org.eclipse.search org.eclipse.core.resources.markers
REQUIRES org.eclipse.search org.eclipse.search.searchPages
REQUIRES org.eclipse.search org.eclipse.search.searchResultSorters
REQUIRES org.eclipse.search org.eclipse.ui.actionSets
REQUIRES org.eclipse.search org.eclipse.ui.views
REQUIRES org.eclipse.ui org.eclipse.core.runtime.applications
REQUIRES org.eclipse.ui org.eclipse.ui.documentProviders
REQUIRES org.eclipse.ui org.eclipse.ui.editors
REQUIRES org.eclipse.ui org.eclipse.ui.elementFactories
REQUIRES org.eclipse.ui org.eclipse.ui.exportWizards
REQUIRES org.eclipse.ui org.eclipse.ui.importWizards
REQUIRES org.eclipse.ui org.eclipse.ui.markerImageProviders
REQUIRES org.eclipse.ui org.eclipse.ui.markerUpdaters
REQUIRES org.eclipse.ui org.eclipse.ui.newWizards
REQUIRES org.eclipse.ui org.eclipse.ui.perspectives
REQUIRES org.eclipse.ui org.eclipse.ui.preferencePages
REQUIRES org.eclipse.ui org.eclipse.ui.propertyPages
REQUIRES org.eclipse.ui org.eclipse.ui.resourceFilters
REQUIRES org.eclipse.ui org.eclipse.ui.views
REQUIRES org.eclipse.update org.eclipse.core.runtime.applications
REQUIRES org.eclipse.update org.eclipse.ui.actionSets
REQUIRES org.eclipse.vcm.core.cvs org.eclipse.vcm.core.adapters
REQUIRES org.eclipse.vcm.core.cvs.ssh org.eclipse.vcm.core.cvs.connectionmethods
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.perspectiveExtensions
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.perspectives
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.popupMenus
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.preferencePages
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.propertyPages
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.viewActions
REQUIRES org.eclipse.vcm.ui org.eclipse.ui.views
REQUIRES org.eclipse.vcm.ui.cvs org.eclipse.ui.newWizards
REQUIRES org.eclipse.vcm.ui.cvs org.eclipse.ui.popupMenus
REQUIRES org.eclipse.vcm.ui.cvs org.eclipse.vcm.core.cvs.authenticator
REQUIRES org.eclipse.vcm.ui.cvs org.eclipse.vcm.ui.adapters
